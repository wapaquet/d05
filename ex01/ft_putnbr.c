#include "ft_putnbr.h"
#include "../../ft_putchar/ft_putchar.h"

void ft_putnbr(int nb)
{
	int i;
	int rest;
	int modulo;
	char str[100];
	
	i = 0;	
	rest = nb;
	modulo = 0;

	if (nb == 0)
	{
		str[i] = '0';
		ft_putchar(str[i]);
		return;
	}

	while (rest)
	{
		modulo = rest % 10;
		rest /= 10;
		str[i] = modulo + 48;
		i++;
	}
	i--;

	while (i >= 0)
	{
		ft_putchar(str[i]);
		i--;
	}
	return;
}